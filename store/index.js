import Vuex from "vuex";

const createStore = () => {
  return new Vuex.Store({
    state: {
      appToken: {},
      loading: false,
      firstRequest: true,
      userInfo: {},
      ballanceInfo: {},
      transaction: [],
      payload: {},
    },
    mutations: {
      setAppToken(state, appToken) {
        state.appToken = appToken;
      },
      setLoading(state, loading) {
        state.loading = loading;
      },
      setUserInfo(state, user) {
        state.userInfo = user;
      },
      setBallanceInfo(state, ballance) {
        state.ballanceInfo = ballance;
      },
      setFirstRequest(state, firstRequest) {
        state.firstRequest = firstRequest;
      },
      setTransaction(state, transaction) {
        state.transaction = transaction;
      },
      setPayload(state, payload) {
        state.payload = payload;
      },
    },
    actions: {
      setAppToken(vuexContext, appToken) {
        vuexContext.commit("setAppToken", appToken);
      },
      setLoading(vuexContext, loading) {
        vuexContext.commit("setLoading", loading);
      },
      setUserInfo(vuexContext, user) {
        vuexContext.commit("setUserInfo", user);
      },
      setBallanceInfo(vuexContext, ballance) {
        vuexContext.commit("setBallanceInfo", ballance);
      },
      setFirstRequest(vuexContext, firstRequest) {
        vuexContext.commit("setFirstRequest", firstRequest);
      },
      setTransaction(vuexContext, transaction) {
        vuexContext.commit("setTransaction", transaction);
      },
      setPayload(vuexContext, payload) {
        vuexContext.commit("setPayload", payload);
      },
    },
    getters: {
      getAppToken(state) {
        return state.appToken;
      },
      getLoading(state) {
        return state.loading;
      },
      getUserInfo(state) {
        return state.userInfo;
      },
      getBallanceInfo(state) {
        return state.ballanceInfo;
      },
      getFirstRequest(state) {
        return state.firstRequest;
      },
      getTransaction(state) {
        return state.transaction;
      },
      getPayload(state) {
        return state.payload;
      },
    },
  });
};

export default createStore;
