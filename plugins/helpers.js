const helpers = {
  login() {
    window.JSLogout();
    let href =
      process.env.HOST_AUTH +
      "/Account/Login/?client_id=" +
      process.env.CLIENT_ID +
      "&redirect_uri=" +
      process.env.HOST_URL +
      "&state=1647905573757";
    window.location.href = href;
  },
  isNumeric(value) {
    return /^-?\d+$/.test(value);
  },
  removeSpace(str) {
    if (!str) return "";
    return str.replace(/\s/g, "");
  },
  phoneValidate(phone) {
    var vnf_regex = /(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})\b/;
    if (vnf_regex.test(phone) == false) {
      return false;
    } else {
      return true;
    }
  },
  regexString(str) {
    if (!str) {
      return null;
    }
    return str.replace(/[^a-zA-Z0-9]/gi, "");
  },
  async getClientIp() {
    return await OauthApp.getData(window.auth_uri + "/infos/getclientip").then(
      (data) => {
        return data;
      }
    );
  },
  async getServerTime() {
    await OauthApp.getData(
      window.auth_uri + "/infos/getservertime?t=" + Date.now()
    ).then((data) => {
      return data;
    });
  },
  paymentContinue() {
    //debugger
    const history = JSON.parse(localStorage.getItem("topupHistory"));
    localStorage.removeItem("topupHistory");
    if (history && history.url) {
      localStorage.removeItem("topupHistory");
      window.location.href = history.url;
    } else {
      window.location.href = "/";
    }
  },
  checkToken(token) {
    if (!token || Object.keys(token).length === 0) {
      return false;
    }
    const tokenParse = JSON.parse(token);
    if (!tokenParse || tokenParse == null) {
      return false;
    }
    const timeNow = new Date();
    const timeToken = tokenParse.expires_in;
    const timeTokenParse = new Date(timeToken * 1000);
    if (timeNow.getTime() > timeTokenParse.getTime()) {
      return false;
    }
    return true;
  },

  checkTokenApp(token) {
    if (token == null || Object.keys(token).length === 0) {
      return false;
    }
    let tokenParse = JSON.parse(token);
    var timeNow = new Date();
    var timeToken = tokenParse.expires_in;
    var timeTokenParse = new Date(timeToken * 1000);
    if (timeNow.getTime() > timeTokenParse.getTime()) {
      return false;
    }
    return true;
  },
  appWriteLog(logData) {
    if (
      window.location.hostname == "localhost" ||
      window.location.href.includes("dev") ||
      window.location.href.includes("testing") ||
      OauthApp.clientIp == "118.70.133.210" ||
      window.location.hostname == "127.0.0.1"
    ) {
      console.log(logData);
    }
  },
  checkAppEnv() {
    if (window.app_env === "production") {
      return false;
    } else {
      return true;
    }
  },
  formatDateDb(date) {
    return [
      date.getFullYear(),
      this.padTo2Digits(date.getMonth() + 1),
      this.padTo2Digits(date.getDate()),
    ].join("-");
  },
  formatDateUser(date) {
    return [
      this.padTo2Digits(date.getDate()),
      this.padTo2Digits(date.getMonth() + 1),
      date.getFullYear(),
    ].join("/");
  },
  padTo2Digits(num) {
    return num.toString().padStart(2, "0");
  },
  formatNumber(num) {
    return num ? num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") : "0";
  },
  logoutHandle(route) {
    window.SSOClient.JSLogout();
    OauthApp.LogOut();
    OauthApp.tokeInfo = null;
    sessionStorage.removeItem("keyForData");
    window.location.href =
      process.env.HOST_AUTH +
      "/Account/LogoutSso?redirect_uri=" +
      process.env.HOST_URL;
  },

  catchHandle() {
    window.location.href = "/";
  },
  checkDateIsToday(popupHistory) {
    if (!popupHistory) {
      return false;
    }
    const now = moment(new Date());
    const history = moment(JSON.parse(popupHistory).time);
    const compareDate = now.diff(history, "days");
    if (compareDate == 0) {
      return true;
    }
    return false;
  },
};

export default helpers;
