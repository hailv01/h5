import appHelpers from "~/plugins/helpers";
export default function ({ store, route, routes, redirect }) {
  //debugger
  store.dispatch("setLoading", true);
  const token = sessionStorage.getItem("keyForData");
  if (!appHelpers.checkToken(token)) {
    window.addEventListener(
      "message",
      (e) => {
        console.log(e);
        if (e.data.key === "keyForData" && e.data.value) {
          store.dispatch("setAppToken", e.data.value);
          store.dispatch("setLoading", false);
          return true;
        }
      },
      false
    );
    setTimeout(() => {
      const token = sessionStorage.getItem("keyForData");
      if (!appHelpers.checkToken(token)) {
        window.location.href = "/Account/Login";
      } else {
        store.dispatch("setLoading", false);
      }
    }, 1000);
  } else {
    store.dispatch("setAppToken", token);
    store.dispatch("setLoading", false);
  }
  return true;
}
