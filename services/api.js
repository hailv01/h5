import axios from "axios";
import appHelpers from "~/plugins/helpers";
const games = {
  async getServer() {
    return await ClientOauthApp.getData(
      window.auth_uri + "/infos/getclientip"
    ).then((data) => {
      console.log(data);
      return data;
    });
  },
  async getUserInfo(token) {
    const tokenParse = JSON.parse(token);
    const payload = {
      jwt: tokenParse.access_token,
      cid: tokenParse.payload.sub,
      client_id: tokenParse.payload.sub,
    };
    var api = window.auth_uri + "/users/get/info";
    return await axios.post(api, payload).then((res) => {
      if (res.data.code == "0") {
        return res.data.data;
      }
      return null;
    });
  },
  async getBalance(token, user) {
    const tokenParse = JSON.parse(token);
    const payload = {
      jwt: tokenParse.access_token,
      cid: tokenParse.payload.sub,
      client_id: tokenParse.payload.sub,
      userId: user.AccountID,
      mty: 10,
    };
    let api = window.auth_uri + "/users/v3/get/balance";
    return await axios
      .post(api, payload)
      .then((res) => {
        if (res.data.code == "0") {
          return res.data.data;
        }
        return null;
      })
      .catch((err) => {
        appHelpers.appWriteLog(err);
        return null;
      });
  },
  async createOrder(token, data) {
    const tokenParse = JSON.parse(token);
    const host = process.env.HOST_URL;
    const api = window.payment_service + "/gate/api/v3/paygate/createorder";
    const params = {
      jwt: tokenParse.access_token,
      cid: tokenParse.payload.sub,
      partnerCode: data.Code, //phương thức thanh toán VCOIN, GATE,...
      moneyType: 10,
      amount: data.Price, //giá VND của vật phẩm đang mua
      vxu: data.Amount, //giá Vxu của vật phẩm đang mua
      makercode: data.Code, //tên dịch vụ ( tên game )
      item_code: data.ID, //id vật phẩm đang mua
      paymentMethod: data.Code, //Mac dinh
      success_uri: host + "/results/billing",
      cancel_uri: host + "/results/failed",
    };
    return await axios
      .post(api, params)
      .then((res) => {
        console.log(res);
        if (res.data.code == "0") {
          const itemTransaction = {
            method: data.Code,
            time: new Date().getTime(),
            amount: data.Price,
            description: data.Description,
          };
          localStorage.setItem(
            "itemTransaction",
            JSON.stringify(itemTransaction)
          );
          return res.data;
        }
        return null;
      })
      .catch((err) => {
        console.log(err);
        return null;
      });
  },
  async getOngPackage(token, method) {
    let apiClientId = window.webshop_uri + "/payment/ong-packages";
    let config = {
      headers: {
        Authorization: JSON.parse(token).access_token,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      params: {
        paymentType: method,
      },
    };
    return await axios
      .get(apiClientId, config)
      .then((res) => {
        appHelpers.appWriteLog(res);
        return res.data.data;
      })
      .catch((err) => {
        console.log(err);
        return null;
      });
  },
  async getGameList(token) {
    const api = process.env.WEBSHOP_URI + "/apps";
    let config = {};
    if (token) {
      config = {
        headers: {
          Authorization: JSON.parse(token).access_token,
        },
      };
    }

    return await axios
      .get(api, config)
      .then((res) => {
        appHelpers.appWriteLog(res);
        return res.data.data;
      })
      .catch((err) => {
        appHelpers.appWriteLog(err);
        return {};
      });
  },

  async getGameServer(token, gameId) {
    const api = process.env.WEBSHOP_URI + "/apps/servers";
    let config = {
      headers: {
        Authorization: JSON.parse(token).access_token,
      },
      params: {
        appId: gameId,
        forPage: "pay",
      },
    };
    return await axios
      .get(api, config)
      .then((res) => {
        appHelpers.appWriteLog(res);
        if (res.data.data) {
          return res.data.data;
        } else {
          return null;
        }
      })
      .catch((err) => {
        appHelpers.appWriteLog(err);
        return null;
      });
  },

  async getGamePopup(appId) {
    const api = process.env.PORTAL_API_URL + "/front/popups";
    let config = {
      headers: {},
      params: {
        position: appId,
        forPage: "nap.vplay.vn",
      },
    };
    return await axios
      .get(api, config)
      .then((res) => {
        appHelpers.appWriteLog(res);
        if (res.data.data) {
          return res.data.data;
        } else {
          return null;
        }
      })
      .catch((err) => {
        appHelpers.appWriteLog(err);
        return null;
      });
  },
  async getCharacter(token, param) {
    let api = window.webshop_uri + "/apps/characters";
    let config = {
      headers: {
        Authorization: JSON.parse(token).access_token,
      },
      params: {
        appId: param.appId,
        serverId: param.serverId,
      },
    };
    return await axios
      .get(api, config)
      .then((res) => {
        appHelpers.appWriteLog(res.data.data);
        return res.data.data;
      })
      .catch((err) => {
        console.log(err);
        return [];
      });
  },
};
export default games;
